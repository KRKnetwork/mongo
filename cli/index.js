const Profiler = require('@krknet/profiler')

const listOp = require('./ops/list')
const createOp = require('./ops/create')
const updateOp = require('./ops/update')
const deleteOp = require('./ops/delete')

module.exports = class CLIservice {
  constructor ({ models = [], dbConnection }) {
    if (!dbConnection) return throwError('No valid dbConnection provided!')

    this.dbConnection = dbConnection
    this.models = models.reduce((acc, d) => ({ ...acc, [d.name.toLowerCase()]: d }), {})
  }

  async query (args) {
    const [model, rawOperation, ...rawParams] = args.length === process.argv.length ? args.slice(2) : args

    if (!model || ['help', '--help', '-h'].includes(model)) return this.showHelp()
    if (!this.models[model]) return throwError(`Model "${model}" is not available!`)
    const operation = resolveOperation(rawOperation)
    if (!operation) return throwError(`Operation "${rawOperation}" is not available!`)

    const params = rawParams.reduce(cleanParams, { _keys: [] })

    await this.dbConnection.connect()
    try {
      await operation(this.models[model], params)
    } catch (err) {
      console.log(err)
    }
    await this.dbConnection.disconnect()
  }

  showHelp () {
    console.log('Usage: node cli [model] [operation]\n')

    console.log('Available Models:')
    const models = Object.keys(this.models).sort((a, b) => a < b ? -1 : 1)
    for (const model of models) console.log(`   ${model}`)
    console.log('')

    console.log('Available Operations:')
    console.log('   list        <l>      *default')
    console.log('   create      <c>')
    console.log('   update      <u>')
    console.log('   delete      <d>')
  }
}

function resolveOperation (param) {
  switch (param) {
    case 'd':
    case 'delete':
      return deleteOp
    case 'u':
    case 'update':
      return updateOp
    case 'c':
    case 'create':
      return createOp
    case 'l':
    case 'ls':
    case 'list':
    case undefined:
      return listOp
    default:
      return false
  }
}

function throwError (msg) {
  Profiler.error(msg)
  process.exit(0) // eslint-disable-line no-process-exit
}

const cleanParams = (acc, d) => {
  if (d.includes('=')) {
    const [rawKey, ...values] = d.split('=')
    acc[rawKey.replace('--', '')] = values.join('=')
  } else {
    acc._keys.push(d)
  }

  return acc
}
