const Form = require('../form')
const Profiler = require('@krknet/profiler')

async function operation (Entity, params) {
  if (Object.keys(params).length > 1) return handlePrefilled(Entity, params)

  const form = new Form(Entity)
  const input = await form.query()

  const excludedKeys = form.settersFields.map(d => d.key)
  const cleanedInput = Object.fromEntries(Object.entries(input).filter(([key, def]) => !excludedKeys.includes(key)))

  const entry = new Entity(cleanedInput)

  for (const key of excludedKeys) {
    await entry[form.schema[key].setter](input[key])
  }

  await entry.save()
  Profiler.success(`Created Entry: ${entry.title || entry.data._id}`)
}

module.exports = operation

async function handlePrefilled (Entity, params) {
  const entry = new Entity(params)
  await entry.save()
  console.log(entry.data._id)
}
