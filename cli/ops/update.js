const Profiler = require('@krknet/profiler')
const Form = require('../form')
const eq = require('enquirer')

async function operation (Entity, params) {
  const entries = await Entity.get()
  if (entries.length === 0) return Profiler.warn('No entries found!')

  let entry
  if (params._keys.length > 0) {
    entry = await Entity.get(params._keys[0])
    if (!entry) return Profiler.warn(`${id} could not be found!`)
  } else {
    const choices = entries.map(d => ({ name: d.data._id, message: d.title }))
    const id = await new eq.Select({ message: 'Pick an entry', choices }).run()

    entry = await Entity.get(id)
  }

  const form = new Form(Entity)
  const input = await form.query(entry.data)

  const excludedKeys = form.settersFields.map(d => d.key)
  const cleanedInput = Object.fromEntries(Object.entries(input).filter(([key, def]) => !excludedKeys.includes(key)))

  entry.patch(cleanedInput)

  for (const key of excludedKeys) {
    if (input[key] !== undefined) await entry[form.schema[key].setter](input[key])
  }

  await entry.save()
  Profiler.success(`Updated Entry: ${entry.title || entry.data._id}`)
}

module.exports = operation
