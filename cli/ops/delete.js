const Profiler = require('@krknet/profiler')
const eq = require('enquirer')

async function operation (Entity, params) {
  Profiler.warn(`Deleting ${Entity.name}`)

  const entries = await Entity.get()
  if (entries.length === 0) return Profiler.warn('No entries found!')

  if (params._keys.length > 0) {
    for (const id of params._keys) {
      const entry = await Entity.get(id)
      if (!entry) {
        Profiler.warn(`${id} could not be found!`)
        continue
      }

      await deleteEntry(entry)
    }
  } else {
    const choices = entries.map(d => ({ name: d.data._id, message: d.title }))
    const id = await new eq.Select({ message: 'Pick an entry', choices }).run()

    const entry = await Entity.get(id)
    await deleteEntry(entry)
  }
}

module.exports = operation

async function deleteEntry (entry) {
  const isConfirmed = await new eq.Confirm({ initial: true, message: `Are you sure, that you want to delete ${entry.title}?` }).run()
  if (!isConfirmed) return Profiler.error('Aborted!')

  await entry.delete()
  Profiler.success(`Deleted Entry: ${entry.title}`)
}
