const eq = require('enquirer')

class Form {
  constructor (Entity) {
    if (!Entity || Entity.name === undefined) throw new Error('A Model is required as Entity!')
    this.title = Entity.name
    this.schema = Entity._modelSchema
  }

  get settersFields () {
    return Object.entries(this.schema)
      .filter(([key, def]) => def.setter)
      .map(([key, def]) => ({ key, def }))
  }

  async query (values = {}) {
    const isNew = values._id === undefined
    const message = isNew ? `Create new ${this.title}` : `Edit ${this.title} ${values._id}`
    console.log(`  ${message}`)

    const results = {}
    for (const [key, def] of Object.entries(this.schema)) {
      const query = getQuery(key, def)
      results[key] = await query({ key, def, results, eq, initial: values[key] }) // .then(d => d).catch(d => )
    }

    return results
  }

  get eq () { return eq }
}

module.exports = Form

function getQuery (key, def) {
  if (def.resolver) return def.resolver

  if (def.type === String && key === 'password') return queryPassword

  switch (def.type) {
    case Boolean:
      return queryBoolean
    case Array:
      return queryArray
    case Date: // ToDo
      return queryDate
    case Number:
      return queryNumber
    case String:
    case RegExp:
    default:
      return queryString
  }
}

function queryArray ({ key, def, initial }) {
  const prompt = new eq.List(getBaseOptions({ key, def, initial }))

  return prompt.run().then(d => d.filter(d => d && d.length > 0))
}

function queryNumber ({ key, def, initial }) {
  const prompt = new eq.NumberPrompt(getBaseOptions({ key, def, initial }))

  return prompt.run()
}

function queryPassword ({ key, def, initial }) {
  const prompt = new eq.Password({
    ...getBaseOptions({ key, def, initial }),
    hint: undefined,
    initial: undefined
  })

  return prompt.run().then(d => d.length > 0 ? d : undefined)
}

function queryBoolean ({ key, def, initial }) {
  const prompt = new eq.Toggle({
    ...getBaseOptions({ key, def, initial }),
    hint: undefined
  })

  return prompt.run()
}

function queryDate ({ key, def, initial }) {
  const baseOptions = getBaseOptions({ key, def, initial })
  baseOptions.initial = !baseOptions.initial ? '' : baseOptions.initial.toISOString()

  const prompt = new eq.Input({ ...baseOptions, validate: value => value.trim().length === 0 ? !def.required : !Number.isNaN(+new Date(value)) })

  return prompt.run().then(d => {
    if (d.trim().length === 0) return null
    const date = new Date(d)
    if (Number.isNaN(+date)) return null
    return date
  })
}

function queryString ({ key, def, initial }) {
  const baseOptions = getBaseOptions({ key, def, initial })
  const prompt = def.options ? new eq.Select({ ...baseOptions, choices: def.options }) : new eq.Input({ ...baseOptions, validate: value => !def.required || (value && value.length > 0) })

  return prompt.run()
}

const getBaseOptions = ({ key, def, initial }) => {
  return {
    message: def.title || key,
    initial: initial === undefined ? (typeof def.default === 'function' ? def.default() : def.default) : initial,
    hint: def.required === false ? '*' : undefined
  }
}
