module.exports = {
  BaseModel: require('./model/'),
  Schema: require('./model/schema'),
  Connection: require('./connection'),
  CLIservice: require('./cli'),
}
