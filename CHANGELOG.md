# Changelog

## [2.1.4] - 2022-04-16

### Changed
- CLI: BugFix
- Connection: Updated `mongodb`
- Dependency Updates

## [2.1.2] - 2022-01-03

### Changed
- Collection: Insert BugFix

## [2.1.1] - 2022-01-01

### Changed
- Model: Get Array BugFix
- Collection: Patch Array BugFix
- Connection: Updated `mongodb`

## [2.1.0] - 2021-12-12

### Added

- Model: get(Array) to search for keys

### Changed

- Connection: Updated `mongodb` requiring node >= 12.9

## [2.0.5] - 2021-11-18

### Changed

- Collection: handle non existent Collection on Drop
- Schema: FullInput on Validator / Default / Transforms

## [2.0.4] - 2021-07-07

### Changed

- Dependency Updates
- CLI: Delete BugFix

## [2.0.3] - 2021-07-01

### Changed

- Dependency Updates
- Model: Object ID BugFix
- Collection: updateOne Deprecation Change

## [2.0.2] - 2021-05-19

### Changed

- Dependency Updates
- Code Formatting

## [2.0.1] - 2021-04-27

### Added

- Connection: Initiators are reenabled

### Changed

- Collection: `updateOne` returns the new Document

## [2.0.0] - 2021-04-27

Dropped Monk as a Wrapper around the MongoDB Driver due to Negligence.

### Added

- Connection: `connection.collection` as alias for `connection.get`
- Collection: `patch(query, patch)` autowrapped by `{ $set: patch }`
- Collection: `patchOne(query, patch)` autowrapped by `{ $set: patch }`
- Model: `__id` to get the `_id` always as a String

### Changed

- Connection: Swapped Monk for own API over mongodb
- Connection: `new Connection()` now takes the URI as first Parameter: `new Connection(uri, options)`
- Collection: Changed `remove(query, { single: true })` to `removeOne(query)`
- Collection: Changed `update(query, update, { single: true })` to `updateOne(query, update)` which acts as replace
- Model: Renamed `static get model` to `static get collection`

### Removed

- Collection: Removed Function `findOneAndDelete` see `removeOne`
- Collection: Removed Function `findOneAndUpdate` see `patchOne`
- Collection: Removed Function `update` see `patch`
- Collection: Removed Function `bulkWrite` see raw MongoDB-Collection
- Collection: Removed Function `dropIndex` see raw MongoDB-Collection
- Collection: Removed Function `dropIndexes` see raw MongoDB-Collection
- Collection: Removed Function `geoHaystackSearch` see raw MongoDB-Collection
- Collection: Removed Function `geoNear` see raw MongoDB-Collection
- Collection: Removed Function `group` see raw MongoDB-Collection
- Collection: Removed Function `indexes` see raw MongoDB-Collection
- Collection: Removed Function `mapReduce` see raw MongoDB-Collection
- Collection: Removed Function `stats` see raw MongoDB-Collection
