module.exports = class Collection {
  constructor (connection, name) {
    this._connection = connection
    this._name = name

    this._collection = this._connection._db.collection(name)
  }

  get raw () { return this._collection }

  async aggregate (pipeline, options = {}) {
    return this._collection.aggregate(pipeline, options).toArray()
  }

  async count (query = {}) {
    return this._collection.countDocuments(query)
  }

  async createIndex (fields, options = {}) {
    return this._collection.createIndex(fields, options)
  }

  async distinct (field, query = {}) {
    return this._collection.distinct(field, query)
  }

  async drop () {
    return this._collection.drop()
      .catch(err => {
        if (err && err.message === 'ns not found') return 'ns not found'
        throw err
      })
  }

  async find (query = {}, options = {}) {
    return this._collection.find(query, options).toArray()
  }

  async findOne (query = {}, options = {}) {
    return this._collection
      .find(query, options)
      .limit(1)
      .toArray()
      .then(docs => (docs && docs[0]) || null)
  }

  async insert (documents) {
    const isArray = Array.isArray(documents)
    if (isArray && documents.length === 0) return []

    return this._collection[isArray ? 'insertMany' : 'insertOne'](documents)
      .then(res => isArray ? this.find({ _id: { $in: Object.values(res.insertedIds) } }) : this.findOne({ _id: res.insertedId }))
  }

  async remove (query) {
    return this._collection.deleteMany(query)
  }

  async removeOne (query) {
    return this._collection.deleteOne(query)
  }

  async updateOne (query, update) {
    return this._collection
      .findOneAndReplace(query, update, { returnDocument: 'after' })
      .then(doc => {
        if (doc && typeof doc.value !== 'undefined') return doc.value
        if (doc.ok && doc.lastErrorObject && doc.lastErrorObject.n === 0) return null
        return doc
      })
  }

  async patch (query, patch) {
    const update = isWrapped(patch) ? patch : { $set: patch }
    return this._collection
      .updateMany(query, update)
      .then(docs => (docs && docs.result) || docs)
  }

  async patchOne (query, patch) {
    const update = isWrapped(patch) ? patch : { $set: patch }
    return this._collection
      .findOneAndUpdate(query, update)
      .then(doc => {
        if (doc && typeof doc.value !== 'undefined') return doc.value
        if (doc.ok && doc.lastErrorObject && doc.lastErrorObject.n === 0) return null
        return doc
      })
  }
}

const isWrapped = patch => patch && (Array.isArray(patch) || patch.$set || patch.$unset || patch.$rename)
