const { MongoClient } = require('mongodb')
const Profiler = require('@krknet/profiler')

const Collection = require('./collection')

module.exports = class Connection {
  constructor (uri, options) {
    if (!uri) throw new Error('No connection URI provided!')

    this._uri = /^mongodb(\+srv)?:\/\//.test(uri) ? uri : `mongodb://${uri}`
    this._options = readOptions(options)

    this._resetState()
  }

  async connect () {
    if (this._state !== STATE.CLOSED) return

    const profiler = new Profiler('MongoDB-Connection')
    if (this._options.doLog) profiler.start()

    return MongoClient
      .connect(this._uri, { useUnifiedTopology: true })
      .then(async client => {
        this._state = STATE.OPEN
        this._client = client
        this._db = client.db()

        for (const initiatior of this._options.initiatiors) await initiatior(this)
        if (this._options.doLog) profiler.succeed()
      })
      .catch(err => {
        this._resetState()

        if (this._options.doLog) profiler.fail()
        throw new Error(err)
      })
  }

  disconnect () {
    if (!this._state === STATE.CLOSED) return

    return this._client
      .close()
      .then(() => {
        this._resetState()

        if (this._options.doLog) Profiler.success('MongoDB-Connection closed')
      })
  }

  get (...args) { return this.collection(...args) }
  collection (name) {
    if (!this._collections[name]) this._collections[name] = new Collection(this, name)

    return this._collections[name]
  }

  _resetState () {
    this._state = STATE.CLOSED
    this._client = null
    this._db = null
    this._collections = {}
  }
}

function readOptions ({
  doLog = true,
  initiatiors = []
} = {}) {
  return { doLog, initiatiors }
}

const STATE = {
  CLOSED: 'closed',
  // OPENING: 'opening',
  OPEN: 'open'
}
