const Schema = require('./schema')
const deepmerge = require('deepmerge')
const cloneDeep = require('lodash.clonedeep')
const { ObjectID } = require('mongodb')

module.exports = class BaseModel {
  constructor (data = {}, isNew = true) {
    this.data = this._sanitize(data)
    this._isNew = isNew
  }

  static get isObjectID () { return this._modelSchema._id === undefined }
  static get collection () { return undefined }
  static get _modelSchema () { return {} }
  static get schema () { return new Schema(this._baseSchema, this._modelSchema) }

  get title () { return this.constructor.isObjectID ? `${this.data._id}` : this.data._id }
  toJSON () { return cloneDeep(this.data) }

  static get _baseSchema () {
    return new Schema({
      _id: { type: String, protected: true },
      _created: { type: Date, protected: true, default: () => new Date() },
      _lastChange: { type: Date, protected: true, default: () => new Date() }
    })
  }

  static async get (search, key = '_id') {
    const type = getType(search)
    const toModel = input => Array.isArray(input) ? input.map(d => new this(d, false)) : new this(input, false)

    switch (type) {
      case 'undefined':
        return this.collection.find({}).then(toModel) // All Entries
      case 'array':
        return this.collection.find({ [key]: { $in: search } }).then(toModel)
      case 'object':
        if (this.isObjectID && search['_id']) search['_id'] = this.objectID(search['_id'])
        return this.collection.find(search).then(toModel)
      case 'objectID':
      case 'string':
      case 'number':
        // By ID return Single Element
        if (this.isObjectID && key === '_id') search = this.objectID(search)
        let result = await this.collection.findOne({ [key]: search })
        return result === null ? undefined : toModel(result)
      default:
        // Unsupported Fallback
        return []
    }
  }

  update (data) {
    const newData = { ...data }
    for (const key in this.constructor._baseSchema.schema) newData[key] = this.data[key]
    this.data = this._sanitize(newData)
    return this
  }

  patch (data) {
    const id = this.data._id
    this.data = { ...this._sanitize(deepmerge(this.data, data, { arrayMerge: (_destination, source) => source })), _id: id } // Keep ID
    return this
  }

  async delete () {
    await this.constructor.collection.removeOne({ _id: this._id })
    return true
  }

  async save (silent = false) {
    if (!silent) this.data._lastChange = new Date()

    if (this._isNew) {
      return this.constructor.collection
        .insert({ ...this.data })
        .then(newData => {
          this._isNew = false
          this.data = this._sanitize(newData)

          return this
        })
        .catch(err => {
          if (err.code !== 11000) throw err
          throw new Error('_id already set')
        })
    } else {
      this.data = await this.constructor.collection
        .updateOne({ _id: this._id }, stripID(this.data))
        .then(newData => this._sanitize(newData))

      return this
    }
  }

  get _id () {
    if (this.data._id === undefined) return
    if (this.constructor.isObjectID) return this.constructor.objectID(this.data._id)
    return this.data._id
  }

  get __id () {
    if (this.data._id === undefined) return
    return `${this.data._id}`
  }

  _sanitize (data = {}) { return this.constructor.schema.parse(data) }
  static validate (input) { return this.schema.validate(input) }

  static objectID (id) {
    try {
      if (id === null || id === undefined) return ObjectID()
      return typeof id === 'string' ? ObjectID.createFromHexString(id) : id
    } catch (_err) {
      return id
    }
  }
}

const stripID = ({ _id, ...rest }) => rest

const getType = input => {
  const simple = typeof input

  switch (simple) {
    case 'object':
      if (input._bsontype === 'ObjectID') return 'objectID'
      if (Array.isArray(input)) return 'array'
      return 'object'
    default:
      // Unsupported Fallback
      return simple
  }
}
