const deepmerge = require('deepmerge')

module.exports = class Schema {
  constructor (schema, mergeSchema) {
    if (mergeSchema) {
      const rawSchema = schema instanceof Schema ? schema.schema : schema
      const rawMergeSchema = mergeSchema instanceof Schema ? mergeSchema.schema : mergeSchema
      this.schema = deepmerge(rawSchema, rawMergeSchema, { arrayMerge: (destination, source) => source })
    } else {
      this.schema = schema
    }
  }

  validate (input, schema, subpath) {
    try {
      this.parse(input, schema, subpath)
    } catch (error) {
      return { valid: false, error }
    }

    return { valid: true }
  }

  parse (input, schema = this.schema, subpath = [], _fullInput) {
    const fullInput = _fullInput || input
    const output = {}

    for (let [key, value] of Object.entries(schema)) {
      const accessor = [...subpath, key]
      if (value.type !== undefined) { // is Schema
        let hit = input[key]
        if (hit === undefined) {
          const aliases = toArray(value.alias)
          for (const alias of aliases) {
            if (input[alias] !== undefined) {
              hit = input[alias]
              break
            }
          }
        }

        if (hit === undefined) { // not set
          if (value.required === true) throw new Error(`Schema Error: Field "${accessor.join('.')}" is required`)
          output[key] = isFunction(value.default) ? value.default(input, output, fullInput) : value.default
        } else { // set
          let fieldValue = transformType(hit, value.type)
          const transformations = toArray(value.transformations)
          for (const trans of transformations) {
            if (isFunction(trans)) {
              fieldValue = trans(fieldValue, input, fullInput)
            } else if (transformers[trans]) {
              fieldValue = transformers[trans](fieldValue)
            }
          }

          if (value.required === true && (fieldValue === undefined || fieldValue === null)) throw new Error(`Schema Error: Field "${accessor.join('.')}" is required`)

          const validators = toArray(value.validators)
          for (const validator of validators) {
            if (!isFunction(validator)) continue
            if (!validator({ transformedValue: fieldValue, rawValue: hit, fullInput })) throw new Error(`Schema Error: Field "${key}" failed on Validator`)
          }

          output[key] = value.type === Array && value.schema !== undefined ? fieldValue.map((entry, index) => this.parse(entry, value.schema, [...accessor, index])) : fieldValue
        }
      } else { // not Schema
        output[key] = this.parse(input[key] || {}, value, accessor, fullInput)
      }
    }

    return output
  }
}

const isArray = value => Array.isArray(value)

const isFunction = value => typeof value === 'function'

const toArray = value => [].concat(typeof value === 'undefined' ? [] : value)

const transformType = (value, type) => {
  if (isArray(type)) return toArray(value).map(d => transformType(d, type[0]))
  if (type === Array) return toArray(value)
  if (value === null) return value

  switch (type) {
    case RegExp:
      return new RegExp(value, 'i') // eslint-disable-line security/detect-non-literal-regexp
    case Date:
      const date = new Date(/^\d{5,}$/.test(value) ? Number(value) : value)
      return date.getTime() === date.getTime() ? date : null
    case Boolean:
      return !(value === 'false' || value === '0' || !value)
    case Number:
      const val = Number(value)
      return Number.isNaN(val) ? null : val
    case Object:
      return Object(value)
    case String:
      return String(value)
    default:
      return value
  }
}

const transformers = {
  toLowerCase: value => value && isFunction(value.toLowerCase) ? value.toLowerCase() : value,
  toUpperCase: value => value && isFunction(value.toUpperCase) ? value.toUpperCase() : value,
  trim: value => value && isFunction(value.trim) ? value.trim() : value,
  capitalize: value => value && isFunction(value.toLowerCase) ? value.charAt(0).toUpperCase() + value.slice(1).toLowerCase() : value,
  toInteger: value => typeof value === 'number' ? Number.parseInt(value) : value,
  floor: value => typeof value === 'number' ? Math.floor(value) : value,
  ceil: value => typeof value === 'number' ? Math.cleil(value) : value,
  longer0orUndefined: value => typeof value === 'string' ? (value.length === 0 ? undefined : value) : value,
  truthyorUndefined: value => !!value ? value : undefined // eslint-disable-line no-extra-boolean-cast
}
